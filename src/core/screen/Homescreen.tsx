import { Component, h } from 'preact'
import { IStore } from '../../tools/store/IStore'
import { NotificationState } from '../components/notification/NotificationState'
import { Singletons, SingletonKey } from '../../tools/singleton/Singletons'
import './Homescreen.css'
import { IApiService } from '../../services/IApiService'
import Stage from '../components/stage/stage'

export default class HomeScreen extends Component<{}, {}> {
  private notificationStore: IStore<NotificationState>
  private apiServivce: IApiService
  constructor() {
    super()
    this.notificationStore = Singletons.Load<IStore<NotificationState>>(
      SingletonKey.notification,
    )
    this.apiServivce = Singletons.Load<IApiService>(SingletonKey.api)
  }

  render() {
    return (
      <div>
        <div class="flex-grid-thirds">
          <Stage name="Dev integration." />
          <Stage name="Dev deployment." />
          <Stage name="Dev health." />
        </div>
        <div class="flex-grid-thirds">
          <Stage name="Staging integration." />
          <Stage name="Staging deployment." />
          <Stage name="Staging health." />
        </div>
        <div class="flex-grid-thirds">
          <Stage name="Prod health." />
        </div>
      </div>
    )
  }
}

//   componentDidMount() {
//     setTimeout(() => {
//       this.apiServivce.get<void, any>(
//         'app/rest/builds/?locator=buildType:Ci_Merge_development',
//         null,
//         (result) => {
//           const parser = new DOMParser()
//           const doc = parser.parseFromString(result, 'text/xml')
//           console.log(doc)
//         },
//         (error) => {},
//       )
//     }, 1000)
//   }
