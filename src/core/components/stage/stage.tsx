import { Component, h } from 'preact'
import Icon from '../../common/Icon'

export default class Stage extends Component<
  { name: string },
  { status: string }
> {
  constructor() {
    super()
    this.state = {
      status: 'OK',
    }
  }

  componentDidMount() {
    this.changeState(5000)
  }

  changeState(idle: number): void {
    setTimeout(() => {
      this.setState({
        status: this.getStatus(),
      })
      this.changeState(1000 * Math.floor(Math.random() * 20))
    }, idle)
  }

  getStatus(): string {
    const v = Math.floor(Math.random() * 10)
    if (v < 6) {
      return 'OK'
    } else if (v < 9) {
      return 'RUNNING'
    } else if (v > 8) {
      return 'NOK'
    }
    return 'NOK'
  }

  getColor(): string {
    if (this.state.status === 'OK') {
      return 'ok'
    } else if (this.state.status === 'RUNNING') {
      return 'pending'
    } else if (this.state.status === 'NOK') {
      return 'nok'
    }
    return ''
  }

  getIcon(): string {
    if (this.state.status === 'OK') {
      return 'fas fa-check-circle'
    } else if (this.state.status === 'RUNNING') {
      return 'fas fa-circle-notch fa-spin'
    } else if (this.state.status === 'NOK') {
      return 'fas fa-exclamation-circle'
    }
    return ''
  }

  render() {
    return (
      <div class={`col ${this.getColor()}`}>
        <div class="cn">
          <div>
            <div>{this.props.name}</div>
            <div>
              <Icon value={this.getIcon()} />
              {this.state.status}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
