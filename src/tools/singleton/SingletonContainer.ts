import { NotificationState } from "../../core/components/notification/NotificationState";
import { ApiService } from "../../services/ApiService";
import { Store } from "../store/Store";
import { Singletons, SingletonKey } from "./Singletons";

export class SingletonContainer {
  Register(): void {
    Singletons.Register(
      SingletonKey.notification,
      new Store<NotificationState>(undefined)
    );
    Singletons.Register(SingletonKey.api, new ApiService());
  }
}
