import { ApiError } from "./ApiService";

export interface IApiService {
  post<TBody, TResult>(
    route: string,
    body: TBody,
    result: (r: TResult) => void,
    errorCallback: (e: ApiError) => void
  ): void;
  get<TParam, TResut>(
    route: string,
    params: TParam,
    result: (r: TResut) => void,
    errorCallback: (e: ApiError) => void,
    extra?: any
  ): void;
  delete<TData, TResult>(
    route: string,
    data: TData,
    result: (r: TResult) => void,
    errorCallback: (e: ApiError) => void
  ): void;
}
